use crate::id::{GenerationType, IndexType, ComponentIdType, TagIdType};
use crate::entity::Entity;
use crate::component::Component;
use crate::pool::Pool;

const COMPONENT_COUNT: usize = ComponentIdType::BITS as usize;

#[derive(Default, Clone)]
pub struct World {
	generations: Vec<GenerationType>,
	free_indicies: Vec<IndexType>,
	components: Vec<ComponentIdType>,
	pools: [Option<Pool>; COMPONENT_COUNT],
	queries: [Vec<Entity>; COMPONENT_COUNT],
	tags: Vec<TagIdType>,
}

impl World {
	pub fn create_entity(&mut self) -> Entity {
		let idx = match self.free_indicies.pop() {
			Some(idx) => idx,
			None => {
				let idx = self.generations.len();
				self.generations.push(0);
				self.components.push(0);
				self.tags.push(0);
				idx as IndexType
			}
		};

		Entity::new(idx, self.generations[idx as usize])
	}

	pub fn destroy_entity(&mut self, entity: &Entity) {
		if !self.is_alive(entity) { return; }

		self.generations[entity.index()] += 1;
		self.free_indicies.push(entity.index);
		self.tags[entity.index()] = 0;

		if self.components[entity.index()] == 0 { return; }

		for b in 0..ComponentIdType::BITS {
			if self.has_component_id(entity, b) {
				self.pools[b as usize].as_mut().unwrap().release(entity.index());
				self.queries[b as usize].retain(|e| e != entity);
			}
		}
		
		self.components[entity.index()] = 0;
	}

	fn is_alive(&self, entity: &Entity) -> bool {
		entity.generation == self.generations[entity.index()]
	}

	/* ================================================================
	   query functions
	   ================================================================ */

	pub fn query<T: Component>(&mut self) -> Vec<Entity> {
		self.queries[T::INDEX].clone()
	}

	pub fn query_id(&mut self, id: ComponentIdType) -> Vec<Entity> {
		let mut res = self.query_helper(id);
		res.retain(|e| self.has_component_id(e, id));
		res
	}

	pub fn query_id_and_tag(&mut self, id: ComponentIdType, tags: TagIdType) -> Vec<Entity> {
		let mut res = self.query_helper(id);
		res.retain(|e| self.has_component_id(e, id) && self.has_tag(e, tags));
		res
	}

	fn query_helper(&mut self, id: ComponentIdType) -> Vec<Entity> {
		let mut res = vec![];
		if id == 0 { return res; }

		for b in 0..ComponentIdType::BITS {
			if b == 1 {
				res = self.queries[b as usize].clone();
			}
		}

		res
	}

	/* ================================================================
	   tag functions
	   ================================================================ */

	pub fn add_tag(&mut self, entity: &Entity, tag: TagIdType) {
		self.tags[entity.index()] |= tag;
	}

	pub fn has_tag(&mut self, entity: &Entity, tag: TagIdType) -> bool {
		self.tags[entity.index()] & tag == tag
	}

	pub fn remove_tag(&mut self, entity: &Entity, tag: TagIdType) {
		if !self.has_tag(entity, tag) { return; }
		self.tags[entity.index()] ^= tag;
	}

	/* ================================================================
	   component functions
	   ================================================================ */

	pub fn add_component<T: Component>(&mut self, entity: &Entity) -> Option<&mut T> {
		if !self.is_alive(entity) { return None; }
		if !self.has_component_id(entity, T::ID) {
			self.add_component_id(entity, T::ID);
			self.queries[T::INDEX].push(*entity);
		}

		if self.pools[T::INDEX].is_none() { 
			self.pools[T::INDEX] = Some(Pool::new::<T>());	
		}
		
		Some(self.pools[T::INDEX].as_mut().unwrap().aquire::<T>(entity.index()))
	}

	pub fn get_component<T: Component>(&mut self, entity: &Entity) -> Option<&mut T> {
		if !self.is_alive(entity) { return None; }
		if !self.has_component_id(entity, T::ID) { return None; }

		Some(self.pools[T::INDEX].as_mut().unwrap().aquire::<T>(entity.index()))
	}

	pub fn remove_component<T: Component>(&mut self, entity: &Entity) {
		if !self.is_alive(entity) { return; }
		if !self.has_component_id(entity, T::ID) { return; }

		self.remove_component_id(entity, T::ID);
		self.queries[T::INDEX].retain(|e| e != entity);
		self.pools[T::INDEX].as_mut().unwrap().release(entity.index())
	}

	pub fn has_component<T: Component>(&mut self, entity: &Entity) -> bool {
		self.has_component_id(entity, T::ID)
	}

	fn has_component_id(&self, entity: &Entity, id: ComponentIdType) -> bool {
		self.components[entity.index()] & id == id
	}

	fn add_component_id(&mut self, entity: &Entity, id: ComponentIdType) {
		self.components[entity.index()] |= id;
	}

	fn remove_component_id(&mut self, entity: &Entity, id: ComponentIdType) {
		self.components[entity.index()] ^= id;
	}
}

#[cfg(test)]
mod test {

	use super::*;

	#[test]
	fn create_entity() {
		let mut world = World::default();
		assert_eq!(world.create_entity().index, 0);
		assert_eq!(world.create_entity().index, 1);
		assert_eq!(world.create_entity().index, 2);
	}

	#[test]
	fn destroy_entity() {
		let mut world = World::default();
		let e = world.create_entity();
		assert_eq!(e.index, 0);

		world.destroy_entity(&e);
		assert_eq!(world.create_entity().index, 0);
		assert_eq!(world.create_entity().index, 1);
	}
}