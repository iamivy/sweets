use crate::component::Component;
use libc::{free, malloc, realloc, memcpy};
use std::mem::size_of;

pub struct Pool {
	pool: *const u8,
	size: usize,
	count: usize,
	capacity: usize,
	entities: Vec<usize>,
	free_indicies: Vec<usize>,
}

impl Pool {
	pub fn new<T: Component>() -> Self {
		let size = size_of::<T>();
		let capacity = 2;

		Self {
			pool: unsafe { malloc(capacity * size).cast() },
			size,
			count: 0,
			capacity,
			entities: vec![],
			free_indicies: vec![],
		}
	}

	pub fn aquire<T: Component>(&mut self, edx: usize) -> &mut T {
		// Make sure, there is enough space inside the entities vektor
		if edx >= self.entities.len() { self.entities.resize(edx + 1, 0); }
		let idx = self.entities[edx];

		// Check, if the entitiy has a component of this kind
		if idx != 0 {
			return unsafe {
				self.pool
					.add((idx - 1) * self.size)
					.cast_mut()
					.cast::<T>()
					.as_mut()
					.unwrap()
			};
		}

		// Check, if there is a component, than can be reused
		if let Some(idx) = self.free_indicies.pop() {
			self.entities[edx] = idx + 1;

			let ptr = self.aquire(edx);
			*ptr = T::default();
			return ptr;
		}

		// Reallocate if there is not enough space
		if self.count >= self.capacity {
			self.capacity *= 2;
			unsafe {
				self.pool = realloc(
					self.pool.cast_mut().cast(),
					self.capacity * self.size
				).cast();
			}
		}

		// Create a new component
		self.entities[edx] = self.count + 1;
		self.count += 1;

		let ptr = self.aquire(edx);
		*ptr = T::default();
		ptr
	}

	pub fn release(&mut self, edx: usize) {
		if self.entities.len() <= edx { return; }

		let idx = self.entities[edx];

		if idx == 0 { return; }
		self.entities[edx] = 0;
		self.free_indicies.push(idx - 1);
	}
}

impl Drop for Pool {
    fn drop(&mut self) {
        unsafe {
            free(self.pool.cast_mut().cast());
        }
    }
}

impl Clone for Pool {
	fn clone(&self) -> Self {
		let pool = unsafe { malloc(self.capacity * self.size).cast() };
		unsafe { memcpy(pool, self.pool.cast(), self.capacity * self.size) };
		
		Self {
			pool: pool.cast(),
			size: self.size,
			capacity: self.capacity,
			count: self.count,
			entities: self.entities.clone(),
			free_indicies: self.free_indicies.clone(),
		}
	}
}

#[cfg(test)]
mod test {

	use crate::id::ComponentIdType;
	use super::*;

	#[derive(Default, Debug, PartialEq, Component)]
	struct Banana {
		name: String,
	}

	impl Banana {
		fn new(name: &str) -> Self {
			Self { name: name.to_string() }
		}
	}

	#[test]
	fn aquire() {
		let mut pool = Pool::new::<Banana>();
		*pool.aquire::<Banana>(0) = Banana::new("Duck");
		assert_eq!(*pool.aquire::<Banana>(0), Banana::new("Duck"));
		assert_eq!(*pool.aquire::<Banana>(1), Banana::new(""));
		assert_eq!(*pool.aquire::<Banana>(2), Banana::new(""));
		assert_eq!(*pool.aquire::<Banana>(4), Banana::new(""));

		assert_eq!(pool.size, size_of::<Banana>());
		assert_eq!(pool.capacity, 4);
		assert_eq!(pool.count, 4);
		assert_eq!(pool.entities, vec![1, 2, 3, 0, 4]);
		assert_eq!(pool.free_indicies, vec![]);
	}

	#[test]
	fn release() {
		let mut pool = Pool::new::<Banana>();
		pool.aquire::<Banana>(0);
		pool.aquire::<Banana>(1);
		pool.aquire::<Banana>(2);

		pool.release(0);
		pool.aquire::<Banana>(3);

		assert_eq!(pool.capacity, 4);
		assert_eq!(pool.count, 3);
		assert_eq!(pool.entities, vec![0, 2, 3, 1]);

		pool.release(1);

		assert_eq!(pool.entities, vec![0, 0, 3, 1]);
		assert_eq!(pool.free_indicies, vec![1]);
	}

	#[test]
	fn clone() {
		let mut pool = Pool::new::<Banana>();
		*pool.aquire::<Banana>(0) = Banana::new("Duck");
		*pool.aquire::<Banana>(1) = Banana::new("Pizza");

		let mut pool_clone = pool.clone();
		assert_eq!(*pool_clone.aquire::<Banana>(0), Banana::new("Duck"));
		assert_eq!(*pool_clone.aquire::<Banana>(1), Banana::new("Pizza"));

		assert_eq!(pool.size, pool_clone.size);
		assert_eq!(pool.capacity, pool_clone.capacity);
		assert_eq!(pool.count, pool_clone.count);
		assert_eq!(pool.entities, pool_clone.entities);
		assert_eq!(pool.free_indicies, pool_clone.free_indicies);
	}
}