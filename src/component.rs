use crate::id::ComponentIdType;

pub trait Component: Default + Sized + 'static {
    const ID: ComponentIdType;
    const INDEX: usize;
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::Component;

    #[derive(Default, Component)]
    struct Banana;

    #[derive(Default, Component)]
    struct Apple;

    #[test]
    fn unique_indicies_and_ids() {
        assert_eq!(Banana::INDEX, 0);
        assert_eq!(Banana::ID, 1);

        assert_eq!(Apple::INDEX, 1);
        assert_eq!(Apple::ID, 2);

        assert_eq!(Banana::INDEX, 0);
        assert_eq!(Banana::ID, 1);
    }
}