use crate::id::{GenerationType, IndexType};

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Entity {
	pub generation: GenerationType,
	pub index: IndexType,
}

impl Entity {
    pub fn new(index: IndexType, generation: GenerationType) -> Self {
        Entity { index, generation }
    }

    #[inline(always)]
    pub fn index(&self) -> usize {
        self.index as usize
    }
}