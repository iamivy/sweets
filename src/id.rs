pub type GenerationType = u32;
pub type IndexType = u32;
pub type ComponentIdType = u32;
pub type TagIdType = u32;